from .git_api.filter import GitRepositoryDownloader
from .git_api.git import Git
from .inv_index.build import InvertedIndexBuilder
from .logger import LOGGER


class TaskExecutor:
    def __init__(self, master_client, config):
        self._master_client = master_client
        self._config = config
        self._git = Git()

    def _get_result(self, link):
        LOGGER.info("Downloading repository from: %s" % link)
        filter = GitRepositoryDownloader(link)
        return filter.run()

    def task_loop(self):
        while True:
            search_task = self._master_client.get_task_search()
            if search_task is not None:
                search_result = self._git.search_repository(search_task[0],
                                                            page=search_task[1])  # ooonddddrrrreeeiii's iiiiinterface
                self._master_client.finish_search(search_result)
            else:
                clone_task = self._master_client.get_task_clone()
                if clone_task is not None:
                    clone_result = self._get_result(clone_task)
                    self._master_client.finish_clone(clone_result)
                else:
                    builder = InvertedIndexBuilder(self._config)
                    builder.build()
                    self._master_client.finish_build_index()

                    break
