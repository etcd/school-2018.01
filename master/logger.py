import logging
import sys

LOGGER = logging.Logger("master", logging.INFO)
handler = logging.StreamHandler(sys.stderr)
handler.setLevel(logging.INFO)
LOGGER.addHandler(handler)
